import React, { Component, Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import AppLayout from '../../layout/AppLayout';

const MembershipPage = React.lazy(() =>
  import('./membership.js')
);

const QuotesPage = React.lazy(() =>
  import('./quotes')
);

const ChatPage = React.lazy(() =>
  import('./chat')
);

const ViewQuotePage = React.lazy(() =>
  import('./viewQuote')
);


class App extends Component {
  render() {
    const { match } = this.props;

    return (
      <AppLayout>
        <div className="dashboard-wrapper">
          <Suspense fallback={<div className="loading" />}>
            <Switch>
              <Redirect exact from={`${match.url}/`} to={`${match.url}/quotes`} />
              <Route
                path={`${match.url}/membership`}
                render={props => <MembershipPage {...props} />}
              />
              <Route
                path={`${match.url}/quotes`}
                render={props => <QuotesPage {...props} />}
              />
              <Route
                path={`${match.url}/chat`}
                render={props => <ChatPage {...props} />}
              />
              <Route
                path={`${match.url}/view`}
                render={props => <ViewQuotePage {...props} />}
              />
              <Redirect to="/error" />
            </Switch>
          </Suspense>
        </div>
      </AppLayout>
    );
  }
}
const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu;
  return { containerClassnames };
};

export default withRouter(
  connect(
    mapStateToProps,
    {}
  )(App)
);
