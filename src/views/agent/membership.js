import React, { Component, Fragment } from "react";
import { Row, CardTitle } from "reactstrap";
import Breadcrumb from "../../containers/navs/Breadcrumb";
import { Separator, Colxx } from "../../components/common/CustomBootstrap";
import { pricesData } from "../../data/prices"
import PriceCard from "../../components/cards/PriceCard";
import IntlMessages from "../../helpers/IntlMessages";
import { injectIntl } from "react-intl";
import { defaultLocale } from '../../constants/defaultValues'

const locale = localStorage.getItem('currentLanguage') || defaultLocale;
class Prices extends Component {
  render() {
    return (
      <Fragment>
        <Row>
          <Colxx xxs="12">
            <Breadcrumb heading="menu.prices" match={this.props.match} />
            <Separator className="mb-5" />
          </Colxx>
        </Row>
        <Row className="equal-height-container mb-5">
          <Colxx xxs="12" className="text-center">
            <h1>CargoBids Premium</h1>
          </Colxx>
          <Colxx xxs="12" className="text-center mb-4">
            <span>Connecting Airfreight Industry representatives</span>
          </Colxx>
          {
            pricesData[locale].map((item, index) => {
              return (
                <Colxx md="12" lg="6" className="col-item mb-6" key={index}>
                  <div className="container">
                    <PriceCard data={item} />
                  </div>
                </Colxx>
              )
            })
          }
        </Row>
      </Fragment>
    );
  }
}
export default injectIntl(Prices);
