import React, { Component, Fragment } from "react";
import {
  Row,
  Card,
  CardBody
} from "reactstrap";
import Breadcrumb from "../../containers/navs/Breadcrumb";
import { Colxx } from "../../components/common/CustomBootstrap";
import { injectIntl } from "react-intl";
import BidList from "../../components/agent/BidList";

class ViewQuote extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  render() {
    {}
    return (
      <Fragment>
        <Row>
          <Colxx xxs="12">
            <Breadcrumb heading="Bids" match={this.props.match} />
          </Colxx>
        </Row>

        <Row>
          <Colxx xl="12" lg="12" className="mb-4">
            <Card className="d-flex mb-3">
              <CardBody>
                <div className="mb-2">
                  <h5 className="card-title mb-2">Title: Cargo</h5>
                  <p>
                    This is the first description of the Cargo View Quote.
                  </p>
                </div>
                <div className="mb-5">
                  <ul className="list-unstyled">
                    <li>Date: 21/11/19</li>
                    <li>Dest: BOG</li>
                    <li>Origin: FCO</li>
                    <li>PCS: 1</li>
                    <li>Weight: 6677.00</li>
                    <li>VOL: 3.0</li>
                    <li>ReadyOn: 29/11/19</li>
                  </ul>
                </div>
              </CardBody>
            </Card>
          </Colxx>

          <Colxx xl="12" lg="12" className="mb-4">
            <BidList />
          </Colxx>
        </Row>

      </Fragment>
    );
  }
}
export default injectIntl(ViewQuote);
