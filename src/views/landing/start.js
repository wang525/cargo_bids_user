import React, { Component, Fragment } from "react";

import NavBar from "../../components/landing/NavBar";
import VideoArea from "../../components/landing/VideoArea";
import Services from "../../components/landing/Services";
import About from "../../components/landing/About";
import FunFacts from "../../components/landing/FunFacts";
import Testimonials from "../../components/landing/Testimonials";
import WelcomeServices from "../../components/landing/WelcomeServices";
import Contact from "../../components/landing/Contact";
import Footer from "../../components/landing/Footer";

import "../../assets/css/components/landing/color/color-default.css";
import "../../assets/css/components/landing/style.css";
import "../../assets/css/components/landing/responsive.css";

export default class Start extends Component {
  render() {
    return (
      <Fragment>
        <NavBar pageName="home" />
        <VideoArea />
        <Services />
        <About />
        <FunFacts />
        <Testimonials />
        <WelcomeServices />
        <Contact />
        <Footer />
      </Fragment>
    )
  }
}
