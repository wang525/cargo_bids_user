import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import UserLayout from '../../layout/UserLayout';
const Second = React.lazy(() =>
  import(/* webpackChunkName: "second" */ './second')
);
const Login = React.lazy(() =>
  import(/* webpackChunkName: "second" */ './login')
);
const Register = React.lazy(() =>
  import(/* webpackChunkName: "second" */ './register')
);
const ForgotPassword = React.lazy(() =>
  import(/* webpackChunkName: "user-forgot-password" */ './forgot-password')
);
const SecondMenu = ({ match }) => (
  <UserLayout>
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/second`} />
      <Route
        path={`${match.url}/second`}
        render={props => <Second {...props} />}
      />
      <Route
        path={`${match.url}/login`}
        render={props => <Login {...props} />}
      />
      <Route
        path={`${match.url}/register`}
        render={props => <Register {...props} />}
      />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
  </UserLayout>
);
export default SecondMenu;
