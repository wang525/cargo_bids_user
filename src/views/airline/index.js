import React, { Component, Suspense } from 'react';
import { Route, withRouter, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import AppLayout from '../../layout/AppLayout';

const MembershipPage = React.lazy(() =>
  import('./membership.js')
);

const BidPage = React.lazy(() =>
  import('./bids')
);

const ChatPage = React.lazy(() =>
  import('./chat')
);

class App extends Component {
  render() {
    const { match } = this.props;

    return (
      <AppLayout>
        <div className="dashboard-wrapper">
          <Suspense fallback={<div className="loading" />}>
            <Switch>
              <Redirect exact from={`${match.url}/`} to={`${match.url}/bids`} />
              <Route
                path={`${match.url}/membership`}
                render={props => <MembershipPage {...props} />}
              />
              <Route
                path={`${match.url}/bids`}
                render={props => <BidPage {...props} />}
              />
              <Route
                path={`${match.url}/chat`}
                render={props => <ChatPage {...props} />}
              />
              <Redirect to="/error" />
            </Switch>
          </Suspense>
        </div>
      </AppLayout>
    );
  }
}
const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu;
  return { containerClassnames };
};

export default withRouter(
  connect(
    mapStateToProps,
    {}
  )(App)
);
