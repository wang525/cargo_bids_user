import React, { Component } from 'react';
import PropTypes from "prop-types";
import "react-modal-video/css/modal-video.min.css";
import ModalVideo from "react-modal-video";

class VideoArea extends Component {
  constructor() {
    super();
    this.state = {
      isOpen: false
    };
    this.openModal = this.openModal.bind(this);
  }
  openModal() {
    this.setState({ isOpen: true });
  }
  render() {
    return (
      <React.Fragment>
        <section id="home" className="video-area video-bg">
          <div className="diplay-table">
            <div className="display-table-cell">
              <div className="video-inner-content">

                <h1>{this.props.Title}</h1>
                <div className="button__holder">
                  <ModalVideo
                    channel="youtube"
                    isOpen={this.state.isOpen}
                    videoId="OjE_vHA5ZJE"
                    onClose={() =>
                      this.setState({
                        isOpen: false
                      })
                    }
                  />
                  <span
                    onClick={this.openModal}
                    className="plus popup-youtube"
                  >
                  </span>
                </div>

                <p>{this.props.Content}</p>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
VideoArea.propTypes = {
  Title: PropTypes.string,
  Content: PropTypes.string,
}
VideoArea.defaultProps = {
  Title: "II tuo contatto diretto con CARGOBID",
  Content: "See how in 15 seconds",
}
export default VideoArea;
