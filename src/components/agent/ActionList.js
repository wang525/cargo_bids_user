import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { ButtonDropdown, DropdownMenu, DropdownItem, DropdownToggle } from "reactstrap";
import IntlMessages from "../../helpers/IntlMessages";
import { injectIntl } from "react-intl";

class ActionList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownSplitOpen: false,
    };
  }

  toggleSplit =()=> {
    this.setState(prevState => ({
      dropdownSplitOpen: !prevState.dropdownSplitOpen
    }));
  }

  componentDidMount(){

  }

  viewQuote = () => {
    this.props.history.push("/agent/view")
  }

  editQuote = () => {

  }

  deleteQuote = () => {

  }

  render() {
    const { dropdownSplitOpen } = this.state;
    return (
      <div className="float-md-right pt-1">
        <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">
          <ButtonDropdown 
            isOpen={dropdownSplitOpen}
            toggle={this.toggleSplit}>
            <DropdownToggle caret color="primary" size="xs">
              Action
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem onClick={() => this.viewQuote()}>View</DropdownItem>
              <DropdownItem onClick={() => this.editQuote()}>Edit</DropdownItem>
              <DropdownItem onClick={() => this.deleteQuote()}>Delete</DropdownItem>
            </DropdownMenu>
          </ButtonDropdown>
        </div>
      </div>
    );
  }
}

export default withRouter(ActionList);
