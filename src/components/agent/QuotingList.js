import React from "react";
import { Card, CardBody, CardTitle } from "reactstrap";
import ReactTable from "react-table";

import IntlMessages from "../../helpers/IntlMessages";
import Pagination from "../DatatablePagination";
import ActionList from "./ActionList";

import data from "../../data/quoting";

const QuotingList = ({title="Quotes"}) => {
  const columns = [
    {
      Header: "TITLE",
      accessor: "title",
      Cell: props => <p className="list-item-heading">{props.value}</p>
    },
    {
      Header: "DATE",
      accessor: "date",
      Cell: props => <p className="text-muted">{props.value}</p>
    },
    {
      Header: "DEST",
      accessor: "dest",
      Cell: props => <p className="text-muted">{props.value}</p>
    },
    {
      Header: "ORIGIN",
      accessor: "origin",
      Cell: props => <p className="text-muted">{props.value}</p>
    },
    {
      Header: "PCS",
      accessor: "pcs",
      Cell: props => <p className="text-muted">{props.value}</p>
    },
    {
      Header: "WEIGHT",
      accessor: "weight",
      Cell: props => <p className="text-muted">{props.value}</p>
    },
    {
      Header: "VOL",
      accessor: "vol",
      Cell: props => <p className="text-muted">{props.value}</p>
    },
    {
      Header: "Ready on",
      accessor: "readyOn",
      Cell: props => <p className="text-muted">{props.value}</p>
    },
    {
      Header: "",
      accessor: "id",
      Cell: props => <ActionList />
    }
  ];
  return (
    <Card className="h-100">
      <CardBody>
        <CardTitle>
          <IntlMessages id={title} />
        </CardTitle>
        <ReactTable
          defaultPageSize={7}
          data={data.slice(0, 12)}
          columns={columns}
          minRows={0}
          showPageJump={false}
          showPageSizeOptions={false}
          PaginationComponent={Pagination}
        />
      </CardBody>
    </Card>
  );
};
export default QuotingList;
