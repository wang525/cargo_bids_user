export const pricesData = {
    en: [
        {
            title: 'Monthly',
            price: '$10 / mo',
            // detail: 'User/Month',
            link: '#',
            features: [
                'Detailed Workout Plans',
                'Nutrition Guides',
                'Scientific Studies Breakdown',
                'Weekly Members Only Email'
            ]
        },
        {
            title: 'Yearly',
            price: '$100 / yr',
            // detail: 'User/Month Up to 10 Users',
            link: '#',
            features: [
                'Detailed Workout Plans',
                'Nutrition Guides',
                'Scientific Studies Breakdown',
                'Weekly Members Only Email'
            ]
        }
    ],
    es: [
        {
            title: 'Monthly',
            price: '$10 / mo',
            // detail: 'User/Month',
            link: '#',
            features: [
                'Detailed Workout Plans',
                'Nutrition Guides',
                'Scientific Studies Breakdown',
                'Weekly Members Only Email'
            ]
        },
        {
            title: 'Yearly',
            price: '$100 / yr',
            // detail: 'User/Month Up to 10 Users',
            link: '#',
            features: [
                'Detailed Workout Plans',
                'Nutrition Guides',
                'Scientific Studies Breakdown',
                'Weekly Members Only Email'
            ]
        }
    ],
    enrtl: [
        {
            title: 'Monthly',
            price: '$10 / mo',
            // detail: 'User/Month',
            link: '#',
            features: [
                'Detailed Workout Plans',
                'Nutrition Guides',
                'Scientific Studies Breakdown',
                'Weekly Members Only Email'
            ]
        },
        {
            title: 'Yearly',
            price: '$100 / yr',
            // detail: 'User/Month Up to 10 Users',
            link: '#',
            features: [
                'Detailed Workout Plans',
                'Nutrition Guides',
                'Scientific Studies Breakdown',
                'Weekly Members Only Email'
            ]
        }
    ],
}
