const data = [
  {
    title: 'Cargo',
    date: '21/11/19',
    dest: 'BOG',
    origin: 'FCO',
    pcs: 1,
    weight: 6677.00,
    vol: 3.0,
    readyOn: '29/11/19',
    id: 1
  },
  {
    title: 'Cargo',
    date: '21/11/19',
    dest: 'BOG',
    origin: 'FCO',
    pcs: 10,
    weight: 333.00,
    vol: 23.0,
    readyOn: '29/11/19',
    id: 2
  },
  {
    title: 'Cargo',
    date: '21/11/19',
    dest: 'BOG',
    origin: 'FCO',
    pcs: 16,
    weight: 343.00,
    vol: 45.0,
    readyOn: '29/11/19',
    id: 3
  },
  {
    title: 'Cargo',
    date: '21/11/19',
    dest: 'BOG',
    origin: 'FCO',
    pcs: 15,
    weight: 5345.00,
    vol: 56.0,
    readyOn: '29/11/19',
    id: 4
  },
  {
    title: 'Cargo',
    date: '21/11/19',
    dest: 'BOG',
    origin: 'FCO',
    pcs: 21,
    weight: 4543.00,
    vol: 57.0,
    readyOn: '29/11/19',
    id: 5
  },
  {
    title: 'Cargo',
    date: '21/11/19',
    dest: 'BOG',
    origin: 'FCO',
    pcs: 41,
    weight: 3453.00,
    vol: 76.0,
    readyOn: '29/11/19',
    id: 6
  },
  {
    title: 'Cargo',
    date: '21/11/19',
    dest: 'BOG',
    origin: 'FCO',
    pcs: 51,
    weight: 4564.00,
    vol: 89.0,
    readyOn: '29/11/19',
    id: 7
  },
  {
    title: 'Cargo',
    date: '21/11/19',
    dest: 'BOG',
    origin: 'FCO',
    pcs: 81,
    weight: 4563.00,
    vol: 23.0,
    readyOn: '29/11/19',
    id: 8
  },
  {
    title: 'Cargo',
    date: '21/11/19',
    dest: 'BOG',
    origin: 'FCO',
    pcs: 19,
    weight: 2342.00,
    vol: 57.0,
    readyOn: '29/11/19',
    id: 9
  },
  {
    title: 'Cargo',
    date: '21/11/19',
    dest: 'BOG',
    origin: 'FCO',
    pcs: 100,
    weight: 4567.00,
    vol: 45.0,
    readyOn: '29/11/19',
    id: 10
  }
]
export default data
  