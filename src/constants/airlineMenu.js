const data = [
    {
      id: "bids",
      icon: "icofont-ui-calendar",
      label: "Bids",
      to: "/airline/bids",
    },
    {
      id: "chat",
      icon: "icofont-wechat",
      label: "Chat",
      to: "/airline/chat",
    }
  ];
  export default data;
  