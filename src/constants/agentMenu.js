const data = [
    {
      id: "quotes",
      icon: "icofont-ui-calendar",
      label: "Quotes",
      to: "/agent/quotes",
    },
    {
      id: "chat",
      icon: "icofont-wechat",
      label: "Chat",
      to: "/agent/chat",
    }
  ];
  export default data;
  